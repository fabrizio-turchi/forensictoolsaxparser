import codecs

#---	class ParserDebug.py
class ParserDebug:
	def __init__(self, debugFile):
		self.dFileName = debugFile
		self.dFileHandle = codecs.open(self.dFileName, 'w', encoding='utf8')

	def writeDebugFILE(self, data):		    	
		line = "Total PICTURE " + str(data.FILEtotal) + '\n'
		self.dFileHandle.write(line)		
		for i in range(data.FILEtotal):
			line = '[FILE name] ' + data.FILEimageName[i]  + '\n'        
			self.dFileHandle.write(line)        	
	
	
	def closeDebug(self):
		self.dFileHandle.close()
